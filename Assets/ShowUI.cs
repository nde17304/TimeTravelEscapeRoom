﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//gives access to unity's built in UI tools
using UnityEngine.UI; 
public class ShowUI : MonoBehaviour
{
    public Canvas overlayScreen;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider CollisionObject)
    {
        if(CollisionObject.tag == "Player")
        {
            //hides the UI canvas 
            overlayScreen.enabled = true;
            Debug.Log("You Entered the Area");
        }
    }

    void OnTriggerExit(Collider CollisionObject)
    {
        if (CollisionObject.tag == "Player")
        {
            //hides the UI canvas 
            overlayScreen.enabled = false;
            Debug.Log("You Entered the Area");
        }
    }
}
